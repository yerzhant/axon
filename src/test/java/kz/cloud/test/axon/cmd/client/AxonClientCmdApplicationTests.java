package kz.cloud.test.axon.cmd.client;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AxonClientCmdApplicationTests {

	@Test
	public void contextLoads() {
	}

}
