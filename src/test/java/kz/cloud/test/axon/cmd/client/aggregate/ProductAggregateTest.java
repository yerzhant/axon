package kz.cloud.test.axon.cmd.client.aggregate;

import kz.cloud.test.axon.cmd.client.api.CreateProductCommand;
import kz.cloud.test.axon.cmd.client.api.ProductCreatedEvent;
import kz.cloud.test.axon.cmd.client.api.ProductUpdatedEvent;
import kz.cloud.test.axon.cmd.client.api.UpdateProductCommand;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author yerzhan
 */
public class ProductAggregateTest {

    private FixtureConfiguration fixture;

    @Before
    public void setUp() {
        fixture = new AggregateTestFixture<>(ProductAggregate.class);
    }

    @Test
    public void testCreateProduct() {
        fixture.givenNoPriorActivity()
                .when(new CreateProductCommand("123", "New prod"))
                .expectSuccessfulHandlerExecution()
                .expectEvents(new ProductCreatedEvent("123", "New prod"));
    }

    @Test
    public void testUpdateProductName() {
        fixture.given(new ProductCreatedEvent("1", "rr"))
                .when(new UpdateProductCommand("1", "uu"))
                .expectEvents(new ProductUpdatedEvent("1", "uu"));
    }

    @Test
    public void testUpdateProductName2() {
        fixture.given(new ProductCreatedEvent("1", "rr"), new ProductUpdatedEvent("1", "tt"))
                .when(new UpdateProductCommand("1", "uu"))
                .expectEvents(new ProductUpdatedEvent("1", "uu"));
    }

}
