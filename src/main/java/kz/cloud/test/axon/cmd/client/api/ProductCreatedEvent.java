package kz.cloud.test.axon.cmd.client.api;

/**
 *
 * @author yerzhan
 */
public class ProductCreatedEvent {

    public final String id;
    public final String name;

    public ProductCreatedEvent(String id, String name) {
        this.id = id;
        this.name = name;
    }

}
