package kz.cloud.test.axon.cmd.client.api;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

/**
 *
 * @author yerzhan
 */
public class UpdateProductCommand {

    @TargetAggregateIdentifier
    public final String id;
    public final String name;

    public UpdateProductCommand(String id, String name) {
        this.id = id;
        this.name = name;
    }

}
