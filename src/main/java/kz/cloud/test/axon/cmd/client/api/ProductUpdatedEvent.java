package kz.cloud.test.axon.cmd.client.api;

/**
 *
 * @author yerzhan
 */
public class ProductUpdatedEvent {

    public final String id;
    public final String name;

    public ProductUpdatedEvent(String id, String name) {
        this.id = id;
        this.name = name;
    }

}
