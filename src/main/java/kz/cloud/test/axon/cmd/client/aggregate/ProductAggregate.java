package kz.cloud.test.axon.cmd.client.aggregate;

import kz.cloud.test.axon.cmd.client.api.CreateProductCommand;
import kz.cloud.test.axon.cmd.client.api.ProductCreatedEvent;
import kz.cloud.test.axon.cmd.client.api.ProductUpdatedEvent;
import kz.cloud.test.axon.cmd.client.api.UpdateProductCommand;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

/**
 *
 * @author yerzhan
 */
@Aggregate
public class ProductAggregate {

    @AggregateIdentifier
    private String id;

    private String name;

    private ProductAggregate() {
    }

    @CommandHandler
    public ProductAggregate(CreateProductCommand command) {
        apply(new ProductCreatedEvent(command.id, command.name));
    }

    @CommandHandler
    public void handleUpdateProductCommand(UpdateProductCommand command) {
        apply(new ProductUpdatedEvent(command.id, command.name));
    }

    @EventSourcingHandler
    public void handleProductCreatedEvent(ProductCreatedEvent event) {
        id = event.id;
        name = event.name;
    }

}
