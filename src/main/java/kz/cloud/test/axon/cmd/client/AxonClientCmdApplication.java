package kz.cloud.test.axon.cmd.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AxonClientCmdApplication {

	public static void main(String[] args) {
		SpringApplication.run(AxonClientCmdApplication.class, args);
	}
}
