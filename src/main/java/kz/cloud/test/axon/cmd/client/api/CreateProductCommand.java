package kz.cloud.test.axon.cmd.client.api;

/**
 *
 * @author yerzhan
 */
public class CreateProductCommand {

    public final String id;
    public final String name;

    public CreateProductCommand(String id, String name) {
        this.id = id;
        this.name = name;
    }

}
